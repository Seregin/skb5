import React from 'react'
import Iaa from './Iaa'

module.exports = ({ storiesOf, action }) => {
  return storiesOf('Iaa', module)
    .add('default', () => {
      return <Iaa />
    })
    .add('title', () => {
      return <Iaa title='Ну и что ты программист?'  />
    })
}
